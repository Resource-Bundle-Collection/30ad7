# Python 3.6 解决 DLL 加载失败问题

## 问题描述
在使用 Python 3.6 和 PyQt5 时，可能会遇到以下错误：
```
ImportError: DLL load failed: 找不到指定的模块
```
具体表现为在导入 `PyQt5.QtCore` 模块时出现上述错误。

## 解决方案
该资源文件提供了解决此问题的详细步骤和相关文件，帮助用户解决 Python 3.6 环境下 PyQt5 模块加载失败的问题。

### 解决方法
1. **检查 Python 版本**：确保你使用的是 Python 3.6 版本。
2. **安装 PyQt5**：如果尚未安装 PyQt5，请使用以下命令进行安装：
   ```
   pip install PyQt5
   ```
3. **下载并替换 DLL 文件**：
   - 下载资源文件中提供的 `python3.dll` 文件。
   - 将该文件放置在 Python 3.6 的安装目录下，通常为 `C:\Python36`。

### 注意事项
- 确保下载的 `python3.dll` 文件与你的 Python 版本匹配。
- 如果问题仍然存在，请尝试重新安装 Python 3.6 或更新到更高版本的 Python。

通过以上步骤，你应该能够解决 `DLL load failed` 的问题，并顺利使用 PyQt5 进行开发。